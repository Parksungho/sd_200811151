package com.ex.ood;
class Ch3Rectangle
{
	protected int x,y;
	protected int height;
	protected int width;
	public Ch3Rectangle(int x,int y,int w,int h)
	{
		this.x=x;
		this.y=y;
		height=h;
		width=w;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public int getArea()
	{
		return width*height;
	}
	public int getPerimiter()
	{
		return (2*(width+height));
	}
	
}
