package com.ex.ood;
import com.ex.ood.Ch3Rectangle;

class Ch3MutableRectangle extends Ch3Rectangle
{
	public Ch3MutableRectangle(int x,int y,int w,int h)
	{
		super(x,y,w,h);
	}
	public void setSize(int w,int h)
	{
		width=w;
		height=h;
	}
}
