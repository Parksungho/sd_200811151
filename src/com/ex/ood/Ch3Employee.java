package com.ex.ood;
import com.ex.ood.Ch3Person;

class Ch3Employee
{
	private Ch3Person person;
	private int salary;
	public Ch3Employee(Ch3Person p,int sal)
	{
		person=p;
		salary=sal;
	}
	public int getSalary()
	{
		return salary;
	}
	public String getName()
	{
		return person.getName();
	}
	public String getAdd()
	{
		return person.getAdd();
	}
}
