package com.ex.ood;
import com.ex.ood.Automobile;
import java.awt.*;
class Minivan extends Automobile
{
        public Minivan(Color color)
        {
                super(color);
        }
        public int getCapacity()
        {
                return 10;
        }
}
