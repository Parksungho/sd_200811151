package com.ex.ood;
import java.util.Iterator;
import java.util.ArrayList;
public class Ch5ArrayListIterator implements Iterator
{
	ArrayList items;
	int position=0;

	public Ch5ArrayListIterator(ArrayList items)
	{
		this.items=items;
	}
	public Object next()
	{
		Object object =items.get(position);
		position=position+1;
		return object;
	}
	public boolean hasNext()
	{
		if(position>=items.size())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public void remove()
	{
	}
}
