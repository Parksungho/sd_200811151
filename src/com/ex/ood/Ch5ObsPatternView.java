package com.ex.ood;
import java.util.Observer;
import java.util.Observable;
class Ch5ObsPatternView implements Observer
{
	public void update(Observable obs,Object arg)
	{
		System.out.println("Something changed!"+obs+","+arg);
	}
}
