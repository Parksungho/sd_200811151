package com.ex.ood;
import com.ex.ood.Ch3Comparator;

class Ch3Sorter
{
	public void sort(Object[] o,Ch3Comparator p)
	{
		for(int i=0;i<o.length-1;i++)
		{
			int num=p.compare(o[i],o[i+1]);
			if(num<0)
			{
				Object val=o[i];
				o[i]=o[i+1];
				o[i+1]=val;
			}
		}
			
	}
}
