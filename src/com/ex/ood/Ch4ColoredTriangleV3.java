package com.ex.ood;
import java.awt.Point;
import java.awt.Color;

public class Ch4ColoredTriangleV3 extends Ch4TriangleV2
{
        private Color color;
        public Ch4ColoredTriangleV3(Point p1,Point p2,Point p3,Color c)
        {
                super(p1,p2,p3);
                color=c;
        }
	public boolean equals(Object obj)
        {
                if(obj ==null)
			return false;
		if(obj.getClass() != this.getClass())
			return false;
		if(!super.equals(obj))
			return false; 
               	
                Ch4ColoredTriangleV3 otherTriangle=
                        (Ch4ColoredTriangleV3)obj;
                return this.color.equals(otherTriangle.color);
		
       }
}

