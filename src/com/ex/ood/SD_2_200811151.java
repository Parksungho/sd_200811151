package com.ex.ood;
import com.ex.ood.EnhancedRectangle;
import com.ex.ood.Automobile;
import com.ex.ood.Sedan;
import com.ex.ood.Minivan;
import com.ex.ood.SportCar;
import com.ex.ood.Ch3MutableSquare;
import com.ex.ood.Ch3MutableRectangle;
import com.ex.ood.Ch3Rectangle;
import com.ex.ood.Ch3Person;
import com.ex.ood.Ch3Student;
import com.ex.ood.Ch3Employee;
import com.ex.ood.Ch3Sorter;
import com.ex.ood.Ch3IntegerComparator;
import com.ex.ood.Ch3StringComparator;
import com.ex.ood.Ch3Comparator;
import java.util.*;
import java.awt.*;
import com.ex.ood.Ch4ColoredTriangle;
import com.ex.ood.Ch4Triangle;
import com.ex.ood.Ch4TriangleV2;
import com.ex.ood.Ch4ColoredTriangleV2;
import com.ex.ood.Ch4ColoredTriangleV3;
import com.ex.ood.Ch4Refactoring1;
import com.ex.ood.Ch5FixedPointV1;
import com.ex.ood.Ch5FixedPointV2;
import com.ex.ood.Ch5FixedPointV3;
import com.ex.ood.Ch5FixedPointV4;
import com.ex.ood.Ch5ObsPatternModel;
import com.ex.ood.Ch5ObsPatternView;
import com.ex.ood.Ch5ArrayListIterator;
import com.ex.ood.Ch5ArrayLinkedList;
import com.ex.ood.Ch7SingPatternLogger;
import com.ex.ood.Ch7SingPatternLoggerBefore;
import com.ex.ood.Ch7WildTurkey;
import com.ex.ood.Ch7MallardDuck;
import com.ex.ood.Ch7TurkeyAdapter;
import com.ex.ood.Ch7Command;
import com.ex.ood.Ch7Light;
import com.ex.ood.Ch7GarageDoor;
import com.ex.ood.Ch7GarageDoorOpenCommand;
import com.ex.ood.Ch7GarageDoorCloseCommand;
import com.ex.ood.Ch7LightOnCommand;
import com.ex.ood.Ch7LightOffCommand;
import com.ex.ood.Ch7SimpleRemoteControl;
import com.ex.ood.Ch7Pizza;
import com.ex.ood.Ch7CheesePizza;
import com.ex.ood.Ch7SimplePizzaFactory;
import com.ex.ood.Ch7PizzaStore;
import com.ex.ood.Ch8SimpleDrawMain;
import com.ex.ood.Ch8PushCounterPanel;
import java.awt.*;
import javax.swing.*;
import com.ex.ood.GumballMachine;
public class SD_2_200811151
{
	public static void main(String[] args)
	{
		//ch2main1();
		//ch2main2();
		//ch2main3();
		//ch3main1();
		//ch3main2();
		//ch3main3();
		//ch4main1();
		//ch4main2();
		//ch4main3();
		//ch4main4();
		//ch5main1();
		//ch5main2();
		//ch5main3();
		//ch7main1();
		//ch7main2();
		//ch7main3();
		//ch7main4();
		//ch8main1();
		//ch8main2();
		ch8main3();
	}
	public static void ch8main3()
	{
		GumballMachine ma=new GumballMachine(2);

		
		ma.insertQuarter();
		ma.turnCrank();

		System.out.println("turn Crank after eject");
		ma.insertQuarter();
		ma.ejectQuarter();
		ma.turnCrank();

		System.out.println("request eject after turn crank");
		
		ma.insertQuarter();
                ma.turnCrank();
		ma.ejectQuarter();

		ma.insertQuarter();
	}
	
	public static void ch8main2()
	{
		JFrame frame=new JFrame("push Counter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Ch8PushCounterPanel panel=new Ch8PushCounterPanel();
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
	public static void ch8main1()
	{
		Ch8SimpleDrawMain drawFram=new Ch8SimpleDrawMain();
		drawFram.setVisible(true);
	}
	public static void ch7main4()
	{
		Ch7SimplePizzaFactory fac=new Ch7SimplePizzaFactory();
		Ch7PizzaStore store=new Ch7PizzaStore(fac);
		store.orderPizza("cheese");
	}
	public static void ch7main3()
	{
		Ch7Light light=new Ch7Light();
		Ch7GarageDoor door=new Ch7GarageDoor();
		Ch7LightOnCommand lightOn=new Ch7LightOnCommand(light);
		Ch7LightOffCommand lightOff=new Ch7LightOffCommand(light);
		Ch7GarageDoorOpenCommand doorOp=new Ch7GarageDoorOpenCommand(door);
		Ch7GarageDoorCloseCommand doorCl=new Ch7GarageDoorCloseCommand(door);

		
		Ch7SimpleRemoteControl rc=new Ch7SimpleRemoteControl();
		System.out.println("RC set LightOnCommand!!");
		rc.setCommand(lightOn);
		System.out.println("execute!!");
		rc.buttonWasPressed();
		System.out.println("RC set LightOffCommand");
		rc.setCommand(lightOff);
		System.out.println("execute!!");
		rc.buttonWasPressed();

		

		System.out.println("\nRC set GarageOpenDoorCommand!!");
		rc.setCommand(doorOp);
		System.out.println("execute!");
		rc.buttonWasPressed();
		System.out.println("\nRC set GarageCloseDoorCommand!!");
                rc.setCommand(doorCl);
                System.out.println("execute!");
                rc.buttonWasPressed();	
 
	}
	public static void ch7main2()
	{
		System.out.println("Logger Before Test\n");
		Ch7SingPatternLoggerBefore be1=new Ch7SingPatternLoggerBefore();
		Ch7SingPatternLoggerBefore be2=new Ch7SingPatternLoggerBefore();
		Ch7SingPatternLoggerBefore be3=new Ch7SingPatternLoggerBefore();
		Ch7SingPatternLoggerBefore be4=new Ch7SingPatternLoggerBefore();

		be1.readEntireLog();
		be2.readEntireLog();
		be3.readEntireLog();
		be4.readEntireLog();

		System.out.println("\nLogger Test");
		Ch7SingPatternLogger lo1=Ch7SingPatternLogger.getInstance();
		lo1.readEntireLog();
		Ch7SingPatternLogger lo2=Ch7SingPatternLogger.getInstance();
                lo2.readEntireLog();
		Ch7SingPatternLogger lo3=Ch7SingPatternLogger.getInstance();
                lo3.readEntireLog();
		Ch7SingPatternLogger lo4=Ch7SingPatternLogger.getInstance();
                lo4.readEntireLog();

	}
	public static void ch7main1()
	{
		Ch7MallardDuck duck=new Ch7MallardDuck();
		Ch7WildTurkey tur=new Ch7WildTurkey();
		Ch7TurkeyAdapter apa=new Ch7TurkeyAdapter(tur);
		System.out.println("\nduck interface");
		duck.fly();
		duck.quack();
		
		System.out.println("\nturkey interface");
		tur.fly();
		tur.gobble();
	
		System.out.println("\nadapter ");
		apa.fly();
		apa.quack();
	}
	public static void ch5main3()
	{
		Ch5ArrayLinkedList list=new Ch5ArrayLinkedList();
		list.printArrayList();
		list.printLinkedList();
	}
	public static void ch5main2()
	{
		Ch5ObsPatternView view =new Ch5ObsPatternView();
		Ch5ObsPatternModel model=new Ch5ObsPatternModel();
		model.addObserver(view);
		model.changeSomething();
	}
	public static void ch5main1()
	{
		Ch5FixedPointV1 fixp=new Ch5FixedPointV1(5,5);
		System.out.println("독립적인 새로운 클래스 테스트");
		System.out.println("x: "+fixp.getX()+"y: "+fixp.getY());
		System.out.println("set 함수 x , 객체를 이용한 접근 x, immutable					하다.");
						
		System.out.println("\n상속을 통한 버전 테스트");
		Ch5FixedPointV2 fixp2=new Ch5FixedPointV2(5,5);
		System.out.println("x: "+fixp2.getX()+"y: "+fixp2.getY());
		System.out.println("좌표 변경");
		fixp2.x=10;
		System.out.println("x: "+fixp2.getX()+"y: "+fixp2.getY());
		System.out.println("상속 받은 좌표를 객체를 통해 직접 변경 mutable");

		System.out.println("\n 포함관계를 통한 테스트 return getLocation ");
		Ch5FixedPointV3 fixp3=new Ch5FixedPointV3(5,5);
		System.out.println("x: "+fixp3.getX()+"y: "+fixp3.getY());
		System.out.println("\nchange Point" ); 
		Point loc=fixp3.getLocation();
		loc.x=10;
		loc.y=10;
		System.out.println("x: "+fixp3.getX()+"y: "+fixp3.getY());
		
		System.out.println("\n 포함관계를 통한 테스트 return Point self");
		Ch5FixedPointV4 fixp4=new Ch5FixedPointV4(5,5);
		System.out.println("x: "+fixp4.getX()+"y: "+fixp4.getY());
                System.out.println("\nchange Point" );                         
                Point loc2=fixp4.getLocation();
                loc2.x=10;
                loc2.y=10;
                System.out.println("x: "+fixp4.getX()+"y: "+fixp4.getY());
	}
	public static void ch4main4()
	{
		Ch4Refactoring1 hotel=new Ch4Refactoring1();
		System.out.println("getMealCharge Call: "
					+hotel.getMealCharge());
		System.out.println("getMovieCharge Call: "
					+hotel.getMovieCharge());
		System.out.println("getRoomCharge Call: "
					+hotel.getRoomCharge());
		System.out.println("Total charge: "+hotel.getTotalBill());
		
	}
	public static void ch4main3()
	{
		System.out.println("Test 3\n");
		Ch4TriangleV2 t=new Ch4TriangleV2(new Point(0,0),new Point(1,1),
                                new Point(2,2));
                Ch4ColoredTriangleV3 rct=new Ch4ColoredTriangleV3
                (new Point(0,0),new Point(1,1), new Point(2,2),Color.red);
                Ch4ColoredTriangleV3 bct=new Ch4ColoredTriangleV3
                (new Point(0,0),new Point(1,1), new Point(2,2),Color.blue);

                System.out.println("Reflexive Test");
                System.out.println("t.equals(t) is "+t.equals(t));
                System.out.println("rct.equals(rct) is "+rct.equals(rct));
                System.out.println("bct.equals(bct) is "+bct.equals(bct));
                System.out.println("\nSymmetric Test");
                System.out.println("t==rct is "+t.equals(rct));
                System.out.println("rct==t is "+rct.equals(t));
                System.out.println("t==bct is "+t.equals(bct));
                System.out.println("bct==t is "+bct.equals(t));
                System.out.println("\nTransitivity Test");
                System.out.println("rct==t is "+rct.equals(t));
                System.out.println("t==bct is "+t.equals(bct));
                System.out.println("rct==bct is "+rct.equals(bct));
                System.out.println("\nConsistent Test(loop 10)");
		for(int i=1;i<=10;i++)
                {
                        System.out.println("t.equals(t) is "+t.equals(t));
                        System.out.println("rct.equals(rct) is "
                                                +rct.equals(rct));                                      System.out.println("bct.equals(bct) is "
                                                +bct.equals(bct));
                }
                System.out.println("\nnon-null Test");
                System.out.println("t.equals(null) is "+t.equals(null));
                System.out.println("bct.equals(null) is "+bct.equals(null));
                System.out.println("rct.equals(null) is "+rct.equals(null));
	}
	public static void ch4main2()
	{
		System.out.println("Test 2\n");
		Ch4Triangle t=new Ch4Triangle(new Point(0,0),new Point(1,1),
                                new Point(2,2));
                Ch4ColoredTriangleV2 rct=new Ch4ColoredTriangleV2
		(new Point(0,0),new Point(1,1), new Point(2,2),Color.red);
		Ch4ColoredTriangleV2 bct=new Ch4ColoredTriangleV2
                (new Point(0,0),new Point(1,1), new Point(2,2),Color.blue);
		
                System.out.println("Reflexive Test");
                System.out.println("t.equals(t) is "+t.equals(t));
                System.out.println("rct.equals(rct) is "+rct.equals(rct));
		System.out.println("bct.equals(bct) is "+bct.equals(bct));
                System.out.println("\nSymmetric Test");
                System.out.println("t==rct is "+t.equals(rct));
                System.out.println("rct==t is "+rct.equals(t));
		System.out.println("t==bct is "+t.equals(bct));
                System.out.println("bct==t is "+bct.equals(t));
		System.out.println("\nTransitivity Test");
                System.out.println("rct==t is "+rct.equals(t));
                System.out.println("t==bct is "+t.equals(bct));
                System.out.println("rct==bct is "+rct.equals(bct));
                System.out.println("\nConsistent Test(loop 10)");
                for(int i=1;i<=10;i++)
                {
                        System.out.println("t.equals(t) is "+t.equals(t));
                        System.out.println("rct.equals(rct) is "
						+rct.equals(rct));                              	System.out.println("bct.equals(bct) is "
						+bct.equals(bct));
		}
		System.out.println("\nnon-null Test");
                System.out.println("t.equals(null) is "+t.equals(null));
                System.out.println("bct.equals(null) is "+bct.equals(null));
		System.out.println("rct.equals(null) is "+rct.equals(null));
	}
	public static void ch4main1()
	{
		System.out.println("Test 1\n");
		Ch4Triangle t=new Ch4Triangle(new Point(0,0),new Point(1,1),
				new Point(2,2));
		Ch4ColoredTriangle ct=new Ch4ColoredTriangle(new Point(0,0),new 			Point(1,1), new Point(2,2),Color.red);
		System.out.println("\nReflexive Test");
		System.out.println("t.equals(t) is "+t.equals(t));
		System.out.println("ct.equals(ct) is "+ct.equals(ct));
		System.out.println("\nSymmetric Test");
		System.out.println("t==ct is "+t.equals(ct));
		System.out.println("ct==t is "+ct.equals(t));
		System.out.println("\nConsistent Test(loop 10)");
		for(int i=1;i<=10;i++)
		{
			System.out.println("t.equals(t) is "+t.equals(t));
                	System.out.println("ct.equals(ct) is "+ct.equals(ct));
		}
		System.out.println("\nnon-null Test");
		System.out.println("t.equals(null) is "+t.equals(null));
		System.out.println("ct.equals(null) is "+ct.equals(null)); 
	}
	public static void ch3main3()
	{
		Ch3IntegerComparator intCom=new Ch3IntegerComparator();
		Ch3StringComparator strCom=new Ch3StringComparator();
		String[] strArr=new String[]{"니은","디귿","기억"};
		Integer[] intArr=new Integer[]{5,2,4,3,1};
		
		System.out.println("정렬 전\n");
		for(int i=0;i<strArr.length;i++)
			System.out.println(strArr[i]+" ");
		for(int i=0;i<intArr.length;i++)
			System.out.println(intArr[i]+" ");

		Ch3Sorter s=new Ch3Sorter();
		s.sort(intArr,intCom);
		s.sort(strArr,strCom);

		 System.out.println("정렬 후\n");
                for(int i=0;i<strArr.length;i++)
                        System.out.println(strArr[i]+" ");
                for(int i=0;i<intArr.length;i++)
                        System.out.println(intArr[i]+" ");
	}
	public static void ch3main2()
	{
		Ch3Person p1=new Ch3Person("park","Korea");
		Ch3Student st=new Ch3Student(p1,4.0);
		System.out.println("student info\n");
		System.out.println("name: "+st.getName()+" address: "+st.getAdd(					)+" GPA: "+st.getGPA()+"\n");	
		
		Ch3Person p2=new Ch3Person("kim","Korea");
		Ch3Employee em=new Ch3Employee(p2,400);
		System.out.println("employee info\n");           
                System.out.println("name: "+em.getName()+" address: "+
			em.getAdd()+" Salary: "+em.getSalary()+"\n");
	
	}
	public static void ch3main1()
	{
		Ch3MutableSquare sq=new Ch3MutableSquare(0,0,10,10);
		System.out.println("create 10 square!!\n");
		System.out.println("square width "+sq.getWidth()+" height "
				  +sq.getHeight()+"\n");
		sq.setSize(5);
		System.out.println("after 5 setSize!\n");
	        System.out.println("square width "+sq.getWidth()+" height "
                                  +sq.getHeight()+"\n");

		Ch3MutableRectangle re=new Ch3MutableRectangle(0,0,10,5);
		System.out.println("create 10-5 rec!!\n");
                System.out.println("rec width "+re.getWidth()+" height "
                                  +re.getHeight()+"\n");
                re.setSize(20,8);
                System.out.println("after 20-8 setSize!\n");
                System.out.println("rec width "+re.getWidth()+" height "
                                  +re.getHeight()+"\n");
	}
		
	public static void ch2main1()
	{
		EnhancedRectangle rec=new EnhancedRectangle(0,0,10,6);
		Point p=rec.getCenter();
		System.out.println("x: "+p.getX()+" "+"y: "+p.getY());
		rec.setCenter(10,10);
		p=rec.getCenter();
		System.out.println("x: "+p.getX()+" "+"y: "+p.getY());
	}
	public static void ch2main2()
	{
		Automobile[] cars=new Automobile[3];
		cars[0]=new Sedan(Color.black);
		cars[1]=new Minivan(Color.blue);
		cars[2]=new SportCar(Color.red);
		
		int totalCapacity=0;
		for(int i=0;i<cars.length;i++)
		{
			if(cars[i] instanceof Sedan)
                             totalCapacity+=((Sedan)cars[i]).getCapacity();
			else if(cars[i] instanceof Minivan)
	                	totalCapacity+=((Minivan)cars[i]).getCapacity();
	        	else if(cars[i] instanceof SportCar)
			{
		        	totalCapacity+=((SportCar)cars[i])
				.getCapacity();
			}
			else
				totalCapacity+=cars[i].getCapacity();
		}
		System.out.println("Total Capacity: "+totalCapacity);
		
		totalCapacity=0;
		for(int i=0;i<cars.length;i++)
			totalCapacity+=cars[i].getCapacity();
		
		System.out.println("Total Capacity: "+totalCapacity);
	}
	public static void ch2main3()
	{
		ArrayList<Integer> list=new ArrayList<Integer>();
		
		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));

		Iterator<Integer> itr=list.iterator();
		System.out.println("list size: "+list.size()+"\n");
		while(itr.hasNext())
		{
			Integer curInt=itr.next();
			System.out.println(curInt.intValue()+" " );
		}
		list.clear();
		if(list.isEmpty())
			System.out.println("clear success \n");
		
		LinkedList<Integer> lList=new LinkedList<Integer>();
		lList.add(new Integer(11));
		lList.add(new Integer(22));
		lList.add(new Integer(33));

		itr=lList.iterator();
		System.out.println("linked size: "+lList.size()+"\n");
		while(itr.hasNext())
		{
			Integer curInt=itr.next();
			System.out.println(curInt.intValue()+" ");
		}
		lList.clear();
		if(lList.isEmpty())
			System.out.println("clear success \n");
	}
}

