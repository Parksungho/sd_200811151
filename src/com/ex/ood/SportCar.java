package com.ex.ood;
import com.ex.ood.Automobile;
import java.awt.*;
class SportCar extends Automobile
{
        public SportCar(Color color)
        {
                super(color);
        }
        public int getCapacity()
        {
                return 2;
        }
}

