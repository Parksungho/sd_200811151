package com.ex.ood;

public class Ch4Refactoring1
{
	public double getTotalBill()
	{
		return getMovieCharge()+getMealCharge()+getRoomCharge();
	}
	public double getRoomCharge()
	{
		return 100.0;
	}
	public double getMovieCharge()
	{
		return 5.0;
	}
	public double getMealCharge()
	{
		return 10.0;
	}
}
