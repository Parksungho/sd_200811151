package com.ex.ood;

import java.awt.Point;

public class Ch5FixedPointV4
{
	private Point pt;
        public Ch5FixedPointV4(Point p)
        {
                this.pt=p;
        }
        public Ch5FixedPointV4(int x,int y)
        {
                this.pt=new Point(x,y);
        }
        public double getX()
        {
                return pt.getX();
        }
        public double getY()
        {
                return pt.getY();
        }
        public Point getLocation()
        {
                return pt;
        }
}
