package com.ex.ood;
import java.awt.Point;

public class Ch4Triangle
{
	private Point p1,p2,p3;
	public Ch4Triangle(Point p1,Point p2,Point p3)
	{
		this.p1=p1;
		this.p2=p2;
		this.p3=p3;
	}
	
	 public boolean equals(Object obj)
        {
                if(!(obj instanceof Ch4Triangle))
                        return false;

                Ch4Triangle otherTriangle=(Ch4Triangle)obj;
                return (p1.equals(otherTriangle.p1)&&p2.equals(otherTriangle.p2)
			&&p3.equals(otherTriangle.p3));
        }
}
