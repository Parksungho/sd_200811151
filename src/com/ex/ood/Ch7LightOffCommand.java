package com.ex.ood;

public class Ch7LightOffCommand implements Ch7Command
{
        Ch7Light light;
        public Ch7LightOffCommand(Ch7Light light)
        {
                this.light=light;
        }
        public void execute()
        {
                light.off();
	}
}

