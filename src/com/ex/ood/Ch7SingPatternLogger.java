package com.ex.ood;
public class Ch7SingPatternLogger
{
	private static Ch7SingPatternLogger uniqueIns;
	public static int num=0;
	private Ch7SingPatternLogger(){num++;}
	public static Ch7SingPatternLogger getInstance()
	{
		if(uniqueIns==null)
			uniqueIns=new Ch7SingPatternLogger();

		return uniqueIns;
	}
	public void readEntireLog()
	{
		System.out.println("instance number: "+num);
	}
}
