package com.ex.ood;

import java.util.ArrayList;

public abstract class Ch7Pizza
{
	String name;
	String dough;
	String sauce;

	ArrayList toppings=new ArrayList();
	void prepare()
	{
		System.out.println("Pizza preparing..");
	}
	void bake()
	{
		System.out.println("backing..");
	}
	void cut()
	{
		System.out.println("cutting..");
	}
	void box()
	{
		System.out.println("boxing..");
	}
	public String getName()
	{
		return this.name;
	}
}
