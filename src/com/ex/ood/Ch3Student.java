/**
	@author park
	@version 0.0
	
*/
package com.ex.ood;
import com.ex.ood.Ch3Person;
	
class Ch3Student 
{
	private Ch3Person person;
	private double GPA;
	/**
	@param Ch3Person p
	@param double score
	*/
	public Ch3Student(Ch3Person p,double score)
	{
		person=p;
		GPA=score;
	}
	/**
	@return double
	*/
	public double getGPA()
	{
		return GPA;
	}
	/**
        @return String
        */
	public String getName()
	{
		return person.getName();
	}
	/**
        @return String
        */
	public String getAdd()
	{
		return person.getAdd();
	}
} 
