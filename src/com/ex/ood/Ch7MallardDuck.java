package com.ex.ood;
public class Ch7MallardDuck implements Ch7Duck
{
	public void quack()
	{
		System.out.println("MallardDuck quack..");
	}
	public void fly()
	{
		System.out.println("MallardDuck flying");
	}
}
