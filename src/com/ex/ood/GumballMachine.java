package com.ex.ood;

public class GumballMachine
{
	State soldOutState;
	State noQuarterState;
	State hasQuarterState;
	State soldState;

	State state=soldState;
	int count=0;
	
	public GumballMachine(int num)
	{
		soldOutState=new SoldOutState(this);
		noQuarterState=new NoQuarterState(this);
        	hasQuarterState=new HasQuarterState(this);
        	soldState=new SoldState(this);
		this.count=num;
		if(num>0)
		{
			state=noQuarterState;
		}
	}
	public void insertQuarter()
	{
		state.insertQuarter();
	}
	public void ejectQuarter()
	{
		state.ejectQuarter();
	}
	public void turnCrank()
	{
		state.turnCrank();
		state.dispense();
	}
	public void setState(State state)
	{
		this.state=state;
	}
	void releaseBall()
	{
		System.out.println("A gumball comes rolling out slot");
		if(count!=0)
			count-=1;
	}
	int getCount()
	{
		return count;
	}
	State getSoldState()
	{
		return soldState;
	}
	State getSoldOutState()
	{
		return soldOutState;
	}
	State getNoQuarterState()
	{
		return noQuarterState;
	}
	State getHasQuarterState()
	{
		return hasQuarterState;
	}

}
