package com.ex.ood;

public class Ch7TurkeyAdapter implements Ch7Duck
{
	Ch7Turkey turkey;
	public Ch7TurkeyAdapter(Ch7Turkey turkey)
	{
		this.turkey=turkey;
	}
	public void fly()
	{
		turkey.fly();
	}
	public void quack()
	{
		turkey.gobble();
	}
}
