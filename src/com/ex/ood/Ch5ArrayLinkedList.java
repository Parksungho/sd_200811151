package com.ex.ood;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import com.ex.ood.Ch5ArrayListIterator;

public class Ch5ArrayLinkedList
{
	public static void printArrayList()
	{
		List<String> arrList=new ArrayList<String>();
		arrList.add("A");
		arrList.add("B");
		arrList.add("C");
		Iterator<String> arrItr=arrList.iterator();
		System.out.println("arrayIterator");
		while(arrItr.hasNext())
		{
			Object element=arrItr.next();
			System.out.println(element+" ");
		}
		System.out.println("arrayIterator Using headfirst");
		ArrayList aList=(ArrayList)arrList;
		Ch5ArrayListIterator arrListItr=new Ch5ArrayListIterator(aList);
		while(arrListItr.hasNext())
		{
			Object element=arrListItr.next();
			System.out.println(element+" ");
		}
		String s1="A";
		if(contains(s1,aList))
			System.out.printf("contains test: %s contained in arrList",s1);
	}
	public static void printLinkedList()
	{
		List<String> liList=new LinkedList<String>();
		liList.add("X");
		liList.add("Y");
		liList.add("Z");
		Iterator<String> liItr=liList.iterator();
		System.out.println("listIterator");
		while(liItr.hasNext())
		{
			Object element=liItr.next();
			System.out.println(element+" ");
		}
	}
	//public static boolean contains(Object o,java.util.ArrayList iterable){	
	//public static boolean contains(Object o,java.util.Collection iterable){
	public static boolean contains(Object o,Iterable iterable)
	{
		for(Object i:iterable)
			if(i.equals(o))
				return true;
		return false;
	}
}
	

