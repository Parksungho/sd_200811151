package com.ex.ood;
public class Ch7WildTurkey implements Ch7Turkey
{
	public void fly()
	{
		System.out.println("WildTurkey flying a short distance");
	}
	public void gobble()
	{
		System.out.println("WildTurkey gobble..");
	}
}
