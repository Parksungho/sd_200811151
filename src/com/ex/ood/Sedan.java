package com.ex.ood;
import com.ex.ood.Automobile;
import java.awt.*;
class Sedan extends Automobile
{
        public Sedan(Color color)
        {
                super(color);
        }
        public int getCapacity()
        {
                return 5;
        }
}

