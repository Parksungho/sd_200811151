package com.ex.ood;

public interface Ch7Duck
{
	public void quack();
	public void fly();
}
