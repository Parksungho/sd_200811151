package com.ex.ood;
import com.ex.ood.Ch3Rectangle;

class Ch3MutableSquare extends Ch3Rectangle
{
	public Ch3MutableSquare(int x,int y,int w,int h)
	{
		super(x,y,w,h);
	}
	public void setSize(int s)
	{
		width=s;
		height=s;
	}
}
