package com.ex.ood;

public class Ch7LightOnCommand implements Ch7Command
{
	Ch7Light light;
	public Ch7LightOnCommand(Ch7Light light)
	{
		this.light=light;
	}
	public void execute()
	{
		light.on();
	}
	
}
