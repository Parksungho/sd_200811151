package com.ex.ood.ch8.draw3;

import javax.swing.*;
import java.util.*;
import java.awt.*;

public class DrawingCanvas extends JPanel
{
	private Drawing drawing;
	public DrawingCanvas()
	{
		this.drawing=new Drawing();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for(Figure figure : drawing)
		{
			figure.draw(g);
		}
	}
	public void addFigure(Figure newFigure)
	{
		drawing.addFigure(newFigure);
		repaint();
	}
}
