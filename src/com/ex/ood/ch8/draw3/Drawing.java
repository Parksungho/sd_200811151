package com.ex.ood.ch8.draw3;

import java.util.*;
public class Drawing implements Iterable<Figure>
{
	private List<Figure> figures;

	public Drawing()
	{
		figures=new ArrayList<Figure>();
	}
	public void addFigure(Figure newFigure)
	{
		figures.add(newFigure);
	}
	public Iterator<Figure> iterator()
	{
		return figures.iterator();
	}
}
