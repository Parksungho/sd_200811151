package com.ex.ood.ch8.draw3;

import java.awt.event.*;

public class CanvasEditor implements MouseListener
{
	private Figure currentFigure;
	public CanvasEditor(Figure initialFigure)
	{
		this.currentFigure=initialFigure;
	}
	public void setCurrentFigure(Figure f)
	{
		this.currentFigure=f;
	}
	public void mouseClicked(MouseEvent e)
	{
		Figure newFigure=(Figure)currentFigure.clone();
		newFigure.setCenter(e.getX(),e.getY());
		((DrawingCanvas)e.getSource()).addFigure(newFigure);
	}
	public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e)
        {
        }
        public void mousePressed(MouseEvent e)
        {
        }
 	public void mouseReleased(MouseEvent e)
        {
        }
	
}	
