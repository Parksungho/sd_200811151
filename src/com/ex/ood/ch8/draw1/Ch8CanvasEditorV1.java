package com.ex.ood.ch8.draw1;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Ch8CanvasEditorV1 implements ActionListener, MouseListener
{
	private JButton btn;
	public Ch8CanvasEditorV1(JButton button)
	{
		btn=button;
	}
	public void actionPerformed(ActionEvent ae)
	{
		btn=(JButton)ae.getSource();	
	}
	public void mouseClicked(MouseEvent e)
	{
		int x=e.getX();
		int y=e.getY();
		JPanel canvas=(JPanel)e.getSource();
		if(btn.getText().equals("Ellipse"))
			canvas.getGraphics().drawOval(x-30,y-20,60,40);
		else if(btn.getText().equals("Rect"))
			canvas.getGraphics().drawRect(x-30,y-20,60,40);
		else
			canvas.getGraphics().drawRect(x-25,y-25,50,50);
	}
	public void mouseEntered(MouseEvent e)
	{
	}
	public void mouseExited(MouseEvent e)
	{
	}
	public void mousePressed(MouseEvent e)
	{
	}
	public void mouseReleased(MouseEvent e)
	{
	}
}		
