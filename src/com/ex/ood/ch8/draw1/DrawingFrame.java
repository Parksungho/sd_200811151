package com.ex.ood.ch8.draw1;
import java.awt.*;
import javax.swing.*;
import com.ex.ood.ch8.draw1.Ch8CanvasEditorV1;
public class DrawingFrame extends JFrame
{
	public DrawingFrame()
	{
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JComponent drawingCanvas=createDrawingCanvas();
		add(drawingCanvas,BorderLayout.CENTER);
		
		JToolBar toolbar=createToolbar(drawingCanvas);
		add(toolbar,BorderLayout.NORTH);
	}
	private JComponent createDrawingCanvas()
	{
		JComponent drawingCanvas=new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
	private JToolBar createToolbar(JComponent canvas)
	{
		JToolBar toolbar=new JToolBar();
		JButton ellipseButton=new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton=new JButton("Square");
		toolbar.add(squareButton);
		JButton rectButton=new JButton("Rect");
		toolbar.add(rectButton);

		Ch8CanvasEditorV1 canvasEditor=new Ch8CanvasEditorV1(ellipseButton);
		ellipseButton.addActionListener(canvasEditor);
		squareButton.addActionListener(canvasEditor);
		rectButton.addActionListener(canvasEditor);
		canvas.addMouseListener(canvasEditor);
		return toolbar;
	}
}
