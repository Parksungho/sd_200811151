package com.ex.ood.ch8.draw4;

import java.awt.*;

public class Ellipse extends Figure 
{
    public Ellipse(int x, int y, int w, int h)  
    {
        super(x, y, w, h);
    }
    public void drawShape(Graphics g)  
    {
        Rectangle rect = getBoundingRect();
        g.drawOval(rect.x, rect.y, rect.width, rect.height);
    }
}
