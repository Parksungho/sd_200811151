package com.ex.ood.ch8.draw4;

import java.awt.*;

public class Rect extends Figure
{
    public Rect(int x, int y, int w, int h) 
    {
        super(x, y, w, h);
    }
    public void drawShape(Graphics g) 
    {
        Rectangle rect = getBoundingRect();
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
    }
}
