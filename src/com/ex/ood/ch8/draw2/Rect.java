package com.ex.ood.ch8.draw2;

import java.awt.*;

public class Rect extends Figure
{
	public Rect(int x,int y,int w,int h)
	{
		super(x,y,w,h);
	}
	public void draw(Graphics g)
	{
		int width=getWidth();
		int height=getHeight();
		g.drawRect(getCenterX(),getCenterY(),width,
height);
	}
}
