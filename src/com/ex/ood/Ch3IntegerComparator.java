package com.ex.ood;
import com.ex.ood.Ch3Comparator;

class Ch3IntegerComparator implements Ch3Comparator
{
	public int compare(Object o1,Object o2)
	{
		Integer n1=(Integer)o1;
		Integer n2=(Integer)o2;

		return n1.intValue() - n2.intValue();
	}
}
