package com.ex.ood;
import java.awt.Point;
import java.awt.Color;

public class Ch4ColoredTriangle extends Ch4Triangle
{
	private Color color;
	public Ch4ColoredTriangle(Point p1,Point p2,Point p3,Color c)
	{
		super(p1,p2,p3);
		color=c;
	}
	public boolean equals(Object obj)
	{
		if(!(obj instanceof Ch4ColoredTriangle))
			return false;

		Ch4ColoredTriangle otherTriangle=(Ch4ColoredTriangle)obj;
		return super.equals(otherTriangle) && this.color.equals(
				otherTriangle.color);
	}
}

