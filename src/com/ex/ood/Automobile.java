package com.ex.ood;
import java.awt.Color;


abstract class Automobile
{
        private Color color;
        public Automobile(Color clr)
        {
                color=clr;
        }
        public abstract  int getCapacity();
}

